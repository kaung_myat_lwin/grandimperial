var switches = [{
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "A811L,A812L",
    "description": "One Gang One Way Switch(10A 250V)",
    "url": "upload/Switches & SocketsA811L,A812L.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "A821L,A822L",
    "description": "Two Gang One Way Switch(10A 250V)",
    "url": "upload/Switches & SocketsA821L,A822L.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "A831L,A832L",
    "description": "Three Gang One Way Switch(10A, 250V)",
    "url": "upload/Switches & SocketsA831L,A832L.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "A841L",
    "description": "Four Gang One Way Switch(10A, 250V)",
    "url": "upload/Switches & SocketsA841L.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "A811-9H-13",
    "description": "Three pin socket with switch(13A)",
    "url": "upload/Switches & SocketsA811-9H-13.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "A811-90-15",
    "description": "Three pin circular socket with switch(15A)",
    "url": "upload/Switches & SocketsA811-90-15.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "A811-9P",
    "description": "Two Pin universal socket with switch(250V)",
    "url": "upload/Switches & SocketsA811-9P.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "A75TV2",
    "description": "Double TV Socket",
    "url": "upload/Switches & SocketsA75TV2.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "AT 01 75TV",
    "description": "TV, Telephone Socket",
    "url": "upload/Switches & SocketsAT 01 75TV.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "AT 01",
    "description": "American standard single Telephone Socket",
    "url": "upload/Switches & SocketsAT 01.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "A75 TV",
    "description": "Single TV Socket",
    "url": "upload/Switches & SocketsA75 TV.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "A8N-45",
    "description": "45A Double pole switch with Neon",
    "url": "upload/Switches & SocketsA8N-45.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "AD 630",
    "description": "Light Dimmer(630W, 250V)",
    "url": "upload/Switches & SocketsAD 630.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "A8B-3",
    "description": "Bell Switch(250V)",
    "url": "upload/Switches & SocketsA8B-3.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "A8MDN-20",
    "description": "Double Pole Switch(20A)",
    "url": "upload/Switches & SocketsA8MDN-20.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "A8BPA",
    "description": "Bell Switch with \"Do Not Disturb\"(220V)",
    "url": "upload/Switches & SocketsA8BPA.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "A30 EKTH",
    "description": "Key Card Switch(30A,250V)",
    "url": "upload/Switches & SocketsA30 EKTH.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "DX 004",
    "description": "2-4 Unit DB(plastic base box, inside)",
    "url": "upload/Switches & SocketsDX 004.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "DX 006",
    "description": "4-6 Unit DB (plastic base box, inside)",
    "url": "upload/Switches & SocketsDX 006.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "DX 009",
    "description": "7-9 Unit DB(plastic base box, inside)",
    "url": "upload/Switches & SocketsDX 009.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "DX 012",
    "description": "10-12 Unit DB(plastic base box, inside)",
    "url": "upload/Switches & SocketsDX 012.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "DX 032",
    "description": "28-32 Unit DB(plastic base box, inside)",
    "url": "upload/Switches & SocketsDX 032.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "D811L,D812L",
    "description": "One Gang One Way Switch(10A 250V)",
    "url": "upload/Switches & SocketsD811L,D812L.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "D821L,D822L",
    "description": "Two Gang One Way Switch(10A 250V)",
    "url": "upload/Switches & SocketsD821L,D822L.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "D831L, D832L",
    "description": "Three Gang One Way Switch(10A, 250V)",
    "url": "upload/Switches & SocketsD831L, D832L.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "D841L",
    "description": "Four Gang One Way Switch(10A, 250V)",
    "url": "upload/Switches & SocketsD841L.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "D851L",
    "description": "Five Gang One Way Switch",
    "url": "upload/Switches & SocketsD851L.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "D9P",
    "description": "Two Pin Universal socket(10A, 250V)",
    "url": "upload/Switches & SocketsD9P.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "D9P2",
    "description": "Twin Two Pin Universal socket",
    "url": "upload/Switches & SocketsD9P2.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "D811N-9P",
    "description": "Two Pin universal socket with switch and neon(10A, 250V)",
    "url": "upload/Switches & SocketsD811N-9P.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "D811N-9P2",
    "description": "Twin Two Pin Universal socket with switch and Neon(10A, 250V)",
    "url": "upload/Switches & SocketsD811N-9P2.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "D9H-13",
    "description": "Three pin socket(13A, 250V)",
    "url": "upload/Switches & SocketsD9H-13.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "D811N-9H-13",
    "description": "Three pin socket with switch and Neon(13A, 250V)",
    "url": "upload/Switches & SocketsD811N-9H-13.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "D9HZ-13",
    "description": "Three pin multifunction socket",
    "url": "upload/Switches & SocketsD9HZ-13.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "D811N-9HZ-13",
    "description": "Three pin multifunction socket with switch and Neon(13A, 250V)",
    "url": "upload/Switches & SocketsD811N-9HZ-13.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "D811N-90-15",
    "description": "Three pin circular socket with switch(1",
    "url": "upload/Switches & SocketsD811N-90-15.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "D9PHZ-13",
    "description": "Two pin & Three pin multifunction socket",
    "url": "upload/Switches & SocketsD9PHZ-13.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "DF821L-9P3",
    "description": "Thriple two pin universal socket with two gang switch(10A, 250V)",
    "url": "upload/Switches & SocketsDF821L-9P3.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "DF821L-9PHZ",
    "description": "Two pin & Three pin multifunction socket with two gang switch(10A, 250V)",
    "url": "upload/Switches & SocketsDF821L-9PHZ.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "DF811L-9P4",
    "description": "Four Gang two pin universal socket with one gang switch",
    "url": "upload/Switches & SocketsDF811L-9P4.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "DF811L-9P2HZ",
    "description": "Twin two pin universal & three pin multifunction socket with one gang switch(10A, 250V)",
    "url": "upload/Switches & SocketsDF811L-9P2HZ.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "DF9P5",
    "description": "Five gang two pin universal socket(10A, 250V)",
    "url": "upload/Switches & SocketsDF9P5.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "DF9P3HZ",
    "description": "Triple two pin universal socket & three pin multifunction socket",
    "url": "upload/Switches & SocketsDF9P3HZ.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "DD630",
    "description": "Light Dimmer(630W, 250V)",
    "url": "upload/Switches & SocketsDD630.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "D8CY-3",
    "description": "Touch time delay switch (3A, 250V)",
    "url": "upload/Switches & SocketsD8CY-3.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "D727",
    "description": "Shaver Socket(240V)",
    "url": "upload/Switches & SocketsD727.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "D8BPA",
    "description": "Bell Switch with \"Do Not Disturb\"(220V)",
    "url": "upload/Switches & SocketsD8BPA.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "",
    "description": "147x86x34 Base Box (Surface Type)-PC",
    "url": "upload/Switches & Sockets.png"
}, {
    "category": "Switches & Sockets",
    "name": "Junon Switch and Socket",
    "code": "",
    "description": "86x86x34 Base Box (Surface Type)-PC",
    "url": "upload/Switches & Sockets.png"
}];

switches.map(function(sw){
  console.log(sw);
  $('#switches').append(
  '<div class="col-md-4">' +
  '<div class="card product-info text-center">' +
  '<div class="product-description">' +
  '<img class="img-responsive img-products img-center" src="' + sw.url + '" alt="' + sw.name + '" />' +
  '<p class="product-description-title">' + sw.name + '</p>' +
  '<p class="product-description-code">' + sw.code + '</p>' +
  '<p class="product-description-type">' + sw.category + '</p>' +
  '<p class="product-description-text">' + sw.description + '</p>' +
  '</div>' + '</div>' +
  '</div>');
});
