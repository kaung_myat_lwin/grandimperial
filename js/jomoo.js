var jomoos = [
        {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "32145-128,1B2-Z",
            "description": "SINGLE-HANDLE LAVATORY FAUCET(SINGLE MOUNTING HOLE)",
            "url": "upload/Sanitary Wares32145-128,1B2-Z.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "32155-128,1B2-Z",
            "description": "SINGLE-HANDLE MEDIUM-TO-HIGH LAVATORY FAUCET",
            "url": "upload/Sanitary Wares32155-128,1B2-Z.jpg"
        },
        {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "32150-131,1C1-Z",
            "description": "SINGLE-HANDLE LAVATORY FAUCET(SINGLE MOUNTING HOLE)",
            "url": "upload/Sanitary Wares32150-131,1C1-Z.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "32162-131,1C1-Z",
            "description": "SINGLE-HANDLE MEDIUM-TO-HIGH LAVATORY FAUCET",
            "url": "upload/Sanitary Wares32162-131,1C1-Z.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "32157-131,1C1-Z",
            "description": "SINGLE-HANDLE MEDIUM-TO-HIGH LAVATORY FAUCET",
            "url": "upload/Sanitary Wares32157-131,1C1-Z.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "32142-126,1B1-Z",
            "description": "SINGLE-HANDLE LAVATORY FAUCET(SINGLE MOUNTING HOLE)",
            "url": "upload/Sanitary Wares32142-126,1B1-Z.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "32151-131,1C2-I011",
            "description": "SINGLE-HANDLE LAVATORY FAUCET(DOUBLE MOUNTING HOLE)",
            "url": "upload/Sanitary Wares32151-131,1C2-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "3274-065, 1C-I011",
            "description": "SINGLE-HANDLE LAVATORY FAUCET(SINGLE MOUNTING HOLE)",
            "url": "upload/Sanitary Wares3274-065, 1C-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "35123-126, 1B-1",
            "description": "SINGLE-HANDLE BATH & SHOWER FAUCET",
            "url": "upload/Sanitary Wares35123-126, 1B-1.jpg"
        },
        {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "35145-146, 1B-I011",
            "description": "SINGLE-HANDLE BATH & SHOWER FAUCET",
            "url": "upload/Sanitary Wares35145-146, 1B-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "35128-131,1C-I011",
            "description": "SINGLE-HANDLE BATH & SHOWER FAUCET",
            "url": "upload/Sanitary Wares35128-131,1C-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "35121-128, 1B-I011",
            "description": "SINGLE-HANDLE BATH & SHOWER FAUCET",
            "url": "upload/Sanitary Wares35121-128, 1B-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "23009-231,1B-I011",
            "description": "DOUBLE-HANDLE LAVATORY FAUCET",
            "url": "upload/Sanitary Wares23009-231,1B-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "2401-255,1C-1",
            "description": "DOUBLE-HANDLE WALL-MOUNTED KITCHEN FAUCET",
            "url": "upload/Sanitary Wares2401-255,1C-1.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "3404-000, 1C1-I011",
            "description": "HOTEL KITCHEN FAUCET",
            "url": "upload/Sanitary Wares3404-000, 1C1-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "3344-065, 1C1-Z",
            "description": "SINGLE-HANDLE KITCHEN FAUCET",
            "url": "upload/Sanitary Wares3344-065, 1C1-Z.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "3333-050, 1C-I011",
            "description": "SINGLE-HANDLE KITCHEN FAUCET",
            "url": "upload/Sanitary Wares3333-050, 1C-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "33063-022, 1C-I011",
            "description": "SINGLE-HANDLE KITCHEN FAUCET",
            "url": "upload/Sanitary Wares33063-022, 1C-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "3307-050, 1C1-Z",
            "description": "SINGLE-HANDLE KITCHEN FAUCET",
            "url": "upload/Sanitary Wares3307-050, 1C1-Z.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "33054-100, 1B1-Z",
            "description": "SINGLE-HANDLE SPRING KITCHEN FAUCET",
            "url": "upload/Sanitary Wares33054-100, 1B1-Z.jpg"
        },


        {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
            "code": "3666-126, 1C-1",
            "description": "SINGLE-HANDLE BAR SHOWER SET",
            "url": "upload/Sanitary Wares3666-126, 1C-1.jpg"
        },
        {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
            "code": "36174-142, 1B-I011",
            "description": "SINGLE-HANDLE BAR SHOWER SET",
            "url": "upload/Sanitary Wares36174-142, 1B-I011.jpg"
        },
        {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
            "code": "G29031-2B01-I011",
            "description": "WALL-MOUNTED SHOWER SET",
            "url": "upload/Sanitary WaresG29031-2B01-I011.jpg"
        },
        {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
            "code": "G04031-2B02-I011",
            "description": "RAINCAN",
            "url": "upload/Sanitary WaresG04031-2B02-I011.jpg"
        },
        {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
            "code": "G22011-2B01-I011",
            "description": "RAINCAN",
            "url": "upload/Sanitary WaresG22011-2B01-I011.jpg"
        },
        {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
            "code": "G03015-2B05-I011",
            "description": "SHOWER HEAD",
            "url": "upload/Sanitary WaresG03015-2B05-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
            "code": "G01031-2B04-I011",
            "description": "SHOWER HEAD",
            "url": "upload/Sanitary WaresG01031-2B04-I011.jpg"
        },
        {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
            "code": "S82023-2C01-1",
            "description": "THREE-FUNCTION HAND SHOWER WITH SLIDER BAR",
            "url": "upload/Sanitary WaresS82023-2C01-1.jpg"
        },
        {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
            "code": "S27021-2B01-2",
            "description": "BIDET SPRAYER",
            "url": "upload/Sanitary WaresS27021-2B01-2.jpg"
        },


        {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
            "code": "933814-1D-I011",
            "description": "TOWER BAR",
            "url": "upload/Sanitary Wares933814-1D-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
            "code": "933802-1D-I011",
            "description": "SINGLE TUMBLER",
            "url": "upload/Sanitary Wares933802-1D-I011.jpg"
        },
        {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
            "code": "933803-1D-I011",
            "description": "DOUBLE TUMBLER",
            "url": "upload/Sanitary Wares933803-1D-I011.jpg"
        },
        {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
            "code": "933805-1D-I011",
            "description": "SOAP DISH",
            "url": "upload/Sanitary Wares933805-1D-I011.jpg"
        },
        {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
            "code": "933804-1D-I011",
            "description": "SOAP DISH",
            "url": "upload/Sanitary Wares933804-1D-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
            "code": "933807-1D-I011",
            "description": "TISSUE HOLDER",
            "url": "upload/Sanitary Wares933807-1D-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
            "code": "934802-1D-I011",
            "description": "SINGLE TUMBLER",
            "url": "upload/Sanitary Wares934802-1D-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
            "code": "934803-1D-I011",
            "description": "DOUBLE TUMBLER",
            "url": "upload/Sanitary Wares934803-1D-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
            "code": "937003-1D-I011",
            "description": "DOUBLE-LAYER BASKET",
            "url": "upload/Sanitary Wares937003-1D-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
            "code": "933606-1D-I011",
            "description": "TOWEL RING",
            "url": "upload/Sanitary Wares933606-1D-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
            "code": "933609-1D-I011",
            "description": "DOUBLE TOWER BAR",
            "url": "upload/Sanitary Wares933609-1D-I011.jpg"
        },
        {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
            "code": "0618-8Z-I011",
            "description": "DOUBLE-BOWL STAINLESS STEEL SINK",
            "url": "upload/Sanitary Wares0618-8Z-I011.jpg"
        },
        {
            "category": "Sanitary Wares",
            "name": "JOMOO INTELLIGENCE & CERAMIC(CERAMIC)",
            "code": "1166-2-1, 31Z-I011",
            "description": "ONE-PIECE TOILET",
            "url": "upload/Sanitary Wares1166-2-1, 31Z-I011.jpg"
        },
        {
            "category": "Sanitary Wares",
            "name": "JOMOO INTELLIGENCE & CERAMIC(CERAMIC)",
            "code": "1183-2, 31Z-I011",
            "description": "ONE-PIECE TOILET",
            "url": "upload/Sanitary Wares1183-2, 31Z-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO INTELLIGENCE & CERAMIC(CERAMIC)",
            "code": "11129-1, 31Z-I011",
            "description": "ONE-PIECE TOILET",
            "url": "upload/Sanitary Wares11129-1, 31Z-I011.jpg"
        },




        {
            "category": "Sanitary Wares",
            "name": "JOMOO GENERIC HARDWARE ANGLE VALVE",
            "code": "7411-108, 1C-I011",
            "description": "COLD-ONLY ANGLE VALVE",
            "url": "upload/"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO GENERIC HARDWARE ANGLE VALVE",
            "code": "7411-108, 1C-I011",
            "description": "COLD-ONLY ANGLE VALVE",
            "url": "upload/"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO GENERIC HARDWARE ANGLE VALVE",
            "code": "7411-108, 1C-I011",
            "description": "COLD-ONLY ANGLE VALVE",
            "url": "upload/Sanitary Wares7411-108, 1C-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO GENERIC HARDWARE ANGLE VALVE",
            "code": "4411-108, 1C-I011",
            "description": "HOT-ONLY ANGLE VALVE",
            "url": "upload/Sanitary Wares4411-108, 1C-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO GENERIC ANGLE VALVE",
            "code": "7507-016, 1C-I011",
            "description": "COLD-ONLY STRAIGHTWAY VALVE",
            "url": "upload/Sanitary Wares7507-016, 1C-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO GENERIC HARDWARE ANGLE VALVE",
            "code": "7506-016, 1C1-I011",
            "description": "COLD-ONLY STRAIGHTWAY VALVE",
            "url": "upload/Sanitary Wares7506-016, 1C1-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO GENERIC ANGLE VALVE",
            "code": "7411-156,1C-I011",
            "description": "COLD-ONLY ANGLE VALVE",
            "url": "upload/Sanitary Wares7411-156,1C-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO GENERIC ANGLE VALVE",
            "code": "7505-016, 1C1-I011",
            "description": "COLD-ONLY STRAIGHTWAY VALVE",
            "url": "upload/Sanitary Wares7505-016, 1C1-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO GENERIC HARDWARE ANGLE VALVE",
            "code": "7505-016, 1C1-I011",
            "description": "COLD-ONLY STRAIGHTWAY VALVE",
            "url": "upload/Sanitary Wares7505-016, 1C1-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO GENERIC HARDWARE DRAIN",
            "code": "92104-7C-1",
            "description": "SHOWER DRAINER",
            "url": "upload/Sanitary Wares92104-7C-1.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM FURNITURE (CABINET)",
            "code": "A240-011A-1",
            "description": "WALL-MOUNTED CABINET",
            "url": "upload/Sanitary WaresA240-011A-1.jpg"
        },
        {
            "category": "Sanitary Wares",
            "name": "JOMOO SHOWER ROOM & BATHTUB(BATHTUB)",
            "code": "Y036111-1A01-1",
            "description": "WAIST-SHAPED BATHTUB",
            "url": "upload/Sanitary WaresY036111-1A01-1.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO GENERIC HARDWARE COMMERCIAL PRODUCT",
            "code": "8224-004, 1B-I011",
            "description": "BUTTON-PRESS FLUSHING VALVE",
            "url": "upload/Sanitary Wares8224-004, 1B-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
            "code": "7714-183,1C-I011",
            "description": "COLD-ONLY KITCHEN FAUCET",
            "url": "upload/Sanitary Wares7714-183,1C-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
            "code": "7701-238, 1C-I011",
            "description": "COLD-ONLY KITCHEN FAUCET",
            "url": "upload/Sanitary Wares7701-238, 1C-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
            "code": "32221-234, 1B-I011",
            "description": "SINGLE-HANDLE LAVATORY FAUCET (COLD ONLY)",
            "url": "upload/Sanitary Wares32221-234, 1B-I011.jpg"
        },
        {
            "category": "Sanitary Wares",
            "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
            "code": "7901-038,1C1-Z",
            "description": "PURIFIED WATER FAUCET",
            "url": "upload/Sanitary Wares7901-038,1C1-Z.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
            "code": "7713-078, 1C2-I011",
            "description": "COLD-ONLY KITCHEN FAUCET",
            "url": "upload/Sanitary Wares7713-078, 1C2-I011.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO CUBO STYLE",
            "code": "37046-000, 1B-1",
            "description": "BATHTUB SPOUT",
            "url": "upload/Sanitary Wares37046-000, 1B-1.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO CUBO STYLE",
            "code": "39011-128, 1B2-Z",
            "description": "WALL-MOUNTED SINGLE-HANDLE TWO PIECES FAUCET (DRINKABLE)",
            "url": "upload/Sanitary Wares39011-128, 1B2-Z.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO CUBO STYLE",
            "code": "35120-128, 1B-1",
            "description": "SINGLE-HANDLE SHOWER FAUCET",
            "url": "upload/Sanitary Wares35120-128, 1B-1.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO CUBO STYLE",
            "code": "9513-00-I013",
            "description": "CONCEALED WATER TANK",
            "url": "upload/Sanitary Wares9513-00-I013.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "32159-139,1B1-Z",
            "description": "SINGLE-HANDLE LAVATORY FAUCET(SINGLE MOUNTING HOLE)",
            "url": "upload/Sanitary Wares32159-139,1B1-Z.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "32160-139,1B1-Z",
            "description": "SINGLE-HANDLE MEDIUM-TO-HIGH LAVATORY FAUCET",
            "url": "upload/Sanitary Wares32160-139,1B1-Z.jpg"
        }, {
            "category": "Sanitary Wares",
            "name": "JOMOO BATHROOM HARDWARE(FUACET)",
            "code": "32113-093,1B1-Z",
            "description": "SINGLE-HANDLE HIGH FAUCET (SINGLE MOUNTING HOLE)",
            "url": "upload/Sanitary Wares32113-093,1B1-Z.jpg"
        },
        {
            "category": "Sanitary Wares",
            "name": "JOMOO INTELLIGENCE & CERAMIC(CERAMIC)",
            "code": "1311-1, 11Z-I011",
            "description": "WALL-HUNG URINAL",
            "url": "upload/Sanitary Wares1311-1, 11Z-I011.jpg"
        },
    {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM FURNITURE (CABINET)",
        "code": "A240-016B-I011",
        "description": "WALL-MOUNTED CABINET",
        "url": "upload/Sanitary WaresA240-016B-I011.jpg"
    },
    {
        "category": "Sanitary Wares",
        "name": "JOMOO INTELLIGENCE & CERAMIC(CERAMIC)",
        "code": "12103-1, 11P-1",
        "description": "PEDESTAL BASIN",
        "url": "upload/Sanitary Wares12103-1, 11P-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO INTELLIGENCE & CERAMIC(CERAMIC)",
        "code": "1296-1, 01P-I011",
        "description": "UNDER COUNTER BASIN",
        "url": "upload/Sanitary Wares1296-1, 01P-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO INTELLIGENCE & CERAMIC(CERAMIC)",
        "code": "12115-1, 01P-1",
        "description": "UNDER COUNTER BASIN",
        "url": "upload/Sanitary Wares12115-1, 01P-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO INTELLIGENCE & CERAMIC(CERAMIC)",
        "code": "12145-1, 01Z-1",
        "description": "UNDER COUNTER BASIN",
        "url": "upload/Sanitary Wares12145-1, 01Z-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO INTELLIGENCE & CERAMIC(CERAMIC)",
        "code": "1511-1, 01P-1",
        "description": "MOP BASIN",
        "url": "upload/Sanitary Wares1511-1, 01P-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE DRAIN",
        "code": "92105-7C-1",
        "description": "SHOWER DRAINER",
        "url": "upload/Sanitary Wares92105-7C-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE DRAIN",
        "code": "9205-1C1-I013",
        "description": "FLOOR DRAIN",
        "url": "upload/Sanitary Wares9205-1C1-I013.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE DRAIN",
        "code": "9247-7C3-1",
        "description": "FLOOR DRAIN",
        "url": "upload/Sanitary Wares9247-7C3-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE HOSE",
        "code": "H3D30-150301C-I011",
        "description": "PVC SHOWER HOSE",
        "url": "upload/Sanitary WaresH3D30-150301C-I011.jpg"
    },
    {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
        "code": "7301-238,1C1-I011",
        "description": "COLD-ONLY FAUCET",
        "url": "upload/Sanitary Wares7301-238,1C1-I011.jpg"
    },
    {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
        "code": "32217-231,1B-I011",
        "description": "LAVATORY FAUCET(COLD ONLY)",
        "url": "upload/Sanitary Wares32217-231,1B-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE DRAIN",
        "code": "9239-1C-1",
        "description": "FLOOR DRAIN",
        "url": "upload/Sanitary Wares9239-1C-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
        "code": "S107013-2B01-I012",
        "description": "THREE-FUNCTION HAND SHOWER WITH WALL-BRACKET",
        "url": "upload/Sanitary WaresS107013-2B01-I012.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
        "code": "S23105-2C01-2",
        "description": "FIVE-FUNCTION HAND SHOWER WITH WALL-BRACKET",
        "url": "upload/Sanitary WaresS23105-2C01-2.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
        "code": "S69013-2B02-8",
        "description": "THREE-FUNCTION HAND SHOWER WITH WALL-BRACKET",
        "url": "upload/Sanitary WaresS69013-2B02-8.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
        "code": "S82023-2C01-2",
        "description": "THREE-FUNCTION HAND SHOWER WITH WALL-BRACKET",
        "url": "upload/Sanitary WaresS82023-2C01-2.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
        "code": "S99023-2B01-1",
        "description": "THREE-FUNCTION HAND SHOWER WITH WALL-BRACKET",
        "url": "upload/Sanitary WaresS99023-2B01-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(FUACET)",
        "code": "S24075-2B03-9",
        "description": "FIVE-FUNCTION HAND SHOWER WITH WALL-BRACKET",
        "url": "upload/Sanitary WaresS24075-2B03-9.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
        "code": "S120013-2B01-3",
        "description": "FIVE-FUNCTION HAND SHOWER WITH WALL-BRACKET",
        "url": "upload/Sanitary WaresS120013-2B01-3.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
        "code": "S127025-2B01-3",
        "description": "FIVE-FUNCTION HAND SHOWER WITH WALL-BRACKET",
        "url": "upload/Sanitary WaresS127025-2B01-3.jpg"
    }, {
        "category": "LED Products",
        "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
        "code": "37150-000,1C-I011",
        "description": "WALL-MOUNTED SHOWER SET",
        "url": "upload/LED Products37150-000,1C-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
        "code": "S27021-2B01-2",
        "description": "BIDET SPRAYER",
        "url": "upload/Sanitary WaresS27021-2B01-2.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
        "code": "S24075-2B03-8",
        "description": "FIVE-FUNCTION HAND SHOWER WITH SLIDER BAR",
        "url": "upload/Sanitary WaresS24075-2B03-8.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
        "code": "3644-093, 1B1-1",
        "description": "SINGLE-HANDLE BAR SHOWER SET (LCD)",
        "url": "upload/Sanitary Wares3644-093, 1B1-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
        "code": "3640-095,1B-I011",
        "description": "SINGLE-HANDLE BAR SHOWER SET",
        "url": "upload/Sanitary Wares3640-095,1B-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
        "code": "36224-191,1B-1",
        "description": "SINGLE-HANDLE BAR SHOWER SET",
        "url": "upload/Sanitary Wares36224-191,1B-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
        "code": "36284-081,1B-1",
        "description": "SINGLE-HANDLE BAR SHOWER SET",
        "url": "upload/Sanitary Wares36284-081,1B-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(SHOWER)",
        "code": "36295-272,1B-1",
        "description": "DECHL ORINATING BAR SHOWER SET",
        "url": "upload/Sanitary Wares36295-272,1B-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
        "code": "7801-049,1C-I011",
        "description": "COLD-ONLY MULTIFUNCTIONAL FAUCET",
        "url": "upload/Sanitary Wares7801-049,1C-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
        "code": "7218-182,1C-1",
        "description": "WASHING MACHINE FAUCET",
        "url": "upload/Sanitary Wares7218-182,1C-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
        "code": "7203-238,1C-I011",
        "description": "COLD-ONLY WASHING MACHINE FAUCET",
        "url": "upload/Sanitary Wares7203-238,1C-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
        "code": "7104-238,1C1-I011",
        "description": "COLD-ONLY FAUCET",
        "url": "upload/Sanitary Wares7104-238,1C1-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
        "code": "76016-080, 1C-I011",
        "description": "SINGLE-HANDLE LAVATORY FAUCET (COLD ONLY)",
        "url": "upload/Sanitary Wares76016-080, 1C-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
        "code": "32218-232,1B-I011",
        "description": "LAVATORY FAUCET(COLD ONLY)",
        "url": "upload/Sanitary Wares32218-232,1B-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
        "code": "32219-232,1B-I011",
        "description": "LAVATORY FAUCET(COLD ONLY)",
        "url": "upload/Sanitary Wares32219-232,1B-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
        "code": "33055-231,1B-I011",
        "description": "SINGLE-HANDLE KITCHEN FAUCET",
        "url": "upload/Sanitary Wares33055-231,1B-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HAR",
        "code": "",
        "description": "",
        "url": "upload/Sanitary Wares.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
        "code": "34005-231,1B-I011",
        "description": "WALL-MOUNTED LAVATORY FAUCET",
        "url": "upload/Sanitary Wares34005-231,1B-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
        "code": "7707-078,1C-I011",
        "description": "COLD-ONLY KITCHEN FAUCET",
        "url": "upload/Sanitary Wares7707-078,1C-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
        "code": "7703-183,1C2-I011",
        "description": "COLD-ONLY KITCHEN FAUCET",
        "url": "upload/Sanitary Wares7703-183,1C2-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE LAVATORY FAUCET",
        "code": "H6200-080103C-I011",
        "description": "DRAIN PIPE",
        "url": "upload/Sanitary WaresH6200-080103C-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE HOSE",
        "code": "H5140-045101C-I011",
        "description": "FAUCET INLET HOSE",
        "url": "upload/Sanitary WaresH5140-045101C-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE HOSE",
        "code": "H4139-050101C-I011",
        "description": "PLASTIC COATED BRAIDED HOSE",
        "url": "upload/Sanitary WaresH4139-050101C-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE HOSE",
        "code": "H5371-030101C-I011",
        "description": "STAINLESS STEEL BRAIDED HOSE",
        "url": "upload/Sanitary WaresH5371-030101C-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(FUACET)",
        "code": "32198-146,1B2-Z",
        "description": "SINGLE-HANDLE SINGLE MOUNTING HOLE PULLING LAVATORY FAUCET",
        "url": "upload/Sanitary Wares32198-146,1B2-Z.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(FUACET)",
        "code": "32112-093,1B1-Z",
        "description": "SINGLE-HANDLE LAVATORY FAUCET(SINGLE MOUNTING HOLE)",
        "url": "upload/Sanitary Wares32112-093,1B1-Z.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE DRAIN",
        "code": "92135-7C1-1",
        "description": "KITCHEN DRAINER",
        "url": "upload/Sanitary Wares92135-7C1-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE DRAIN",
        "code": "92132-1C-1",
        "description": "SHOWER DRAINER",
        "url": "upload/Sanitary Wares92132-1C-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE DRAIN",
        "code": "9252-1C-1",
        "description": "KITCHEN DRAINER",
        "url": "upload/Sanitary Wares9252-1C-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE DRAIN",
        "code": "9210-1C1-I012",
        "description": "WASHING MACHINE DRAINER",
        "url": "upload/Sanitary Wares9210-1C1-I012.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE DRAIN",
        "code": "9244-1C1-I011",
        "description": "FLOOR DRAIN",
        "url": "upload/Sanitary Wares9244-1C1-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE DRAIN",
        "code": "9245-7C2-1",
        "description": "WASHING MACHINE DRAINER",
        "url": "upload/Sanitary Wares9245-7C2-1.jpg"
    },
    {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE DRAIN",
        "code": "92131-1C-1",
        "description": "WASHING MACHINE DRAINER",
        "url": "upload/Sanitary Wares92131-1C-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO GENERIC HARDWARE DRAIN",
        "code": "92130-1C-1",
        "description": "FLOOR DRAIN",
        "url": "upload/Sanitary Wares92130-1C-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO INTELLIGENCE & CERAMIC(CERAMIC)",
        "code": "J13037-1,21S-1",
        "description": "FLOOR-TYPE SENSOR URINAL",
        "url": "upload/Sanitary WaresJ13037-1,21S-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO INTELLIGENCE & CERAMIC(CERAMIC)",
        "code": "1179-1,31S-I011",
        "description": "ONE-PIECE TOILET",
        "url": "upload/Sanitary Wares1179-1,31S-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO INTELLIGENCE & CERAMIC(CERAMIC)",
        "code": "11117-1,31Z-1",
        "description": "ONE-PIECE TOILET",
        "url": "upload/Sanitary Wares11117-1,31Z-1.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO INTELLIGENCE & CERAMIC(CERAMIC)",
        "code": "11158-2-1,11Z-I011",
        "description": "TWO-PIECE TOILET",
        "url": "upload/Sanitary Wares11158-2-1,11Z-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
        "code": "934114-1D-I011",
        "description": "DOUBLE-LAYER CORNER SHELF",
        "url": "upload/Sanitary Wares934114-1D-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
        "code": "934115-1D-I011",
        "description": "SINGLE-LAYER CORNER SHELF",
        "url": "upload/Sanitary Wares934115-1D-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
        "code": "937104-1D-I011",
        "description": "SINGLE-LAYER BASKET",
        "url": "upload/Sanitary Wares937104-1D-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
        "code": "933605-1D-I011",
        "description": "SOAP DISH",
        "url": "upload/Sanitary Wares933605-1D-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
        "code": "933602-1D-I011",
        "description": "SINGLE TUMBLER",
        "url": "upload/Sanitary Wares933602-1D-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
        "code": "933603-1D-I011",
        "description": "DOUBLE TUMBLER",
        "url": "upload/Sanitary Wares933603-1D-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
        "code": "933604-1D-I011",
        "description": "SOAP DISH",
        "url": "upload/Sanitary Wares933604-1D-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
        "code": "933607-1D-I011",
        "description": "TISSUE HOLDER",
        "url": "upload/Sanitary Wares933607-1D-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
        "code": "933611-1D-I011",
        "description": "TOILET BRUSH HOLDER",
        "url": "upload/Sanitary Wares933611-1D-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
        "code": "933601-1D-I011",
        "description": "ROBE HOOK",
        "url": "upload/Sanitary Wares933601-1D-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
        "code": "933613-1D-I011",
        "description": "SWINGABLE TOWEL SHELF",
        "url": "upload/Sanitary Wares933613-1D-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
        "code": "933610-1D-I011",
        "description": "GLASS SHELF",
        "url": "upload/Sanitary Wares933610-1D-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
        "code": "933612-1D-I011",
        "description": "TOWEL SHELF",
        "url": "upload/Sanitary Wares933612-1D-I011.jpg"
    }, {
        "category": "Sanitary Wares",
        "name": "JOMOO BATHROOM HARDWARE(ACCESSORIES)",
        "code": "933608-1D-I011",
        "description": "SINGLE TOWEL BAR",
        "url": "upload/Sanitary Wares933608-1D-I011.jpg"
    }
];

function transformLetters(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
};

jomoos.map(function(jomoo) {
    $('#jomoo').append(
        '<div class="col-md-4">' +
        '<div class="card product-info text-center">' +
        '<div class="product-description">' +
        '<img src="' + jomoo.url + '" alt="' + jomoo.name + '" class="img-responsive img-center img-products"/>' +
        '<p class="product-description-title">' + transformLetters(jomoo.name) + '</p>' +
        '<p class="product-description-code">' + jomoo.code + '</p>' +
        '<p class="product-description-type">' + jomoo.category + '</p>' +
        '<p class="product-description-text">' + transformLetters(jomoo.description) + '</p>' +
        '</div>' + '</div>' +
        '</div>');
});
