var leds = [{
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "P0010-600",
    "description": "product-size(D-600),watts(48w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsP0010-600.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "PL018-350",
    "description": "product-size(D-350),watts(36w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsPL018-350.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA005-1",
    "description": "product-size(70*49*70),watts(1w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA005-1.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA005-2",
    "description": "product-size(135*49*70),watts(2*1w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA005-2.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA501",
    "description": "product-size(60*40*80),watts(1w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA501.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA501-2",
    "description": "product-size(60*40*80),watts(2*1w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA501-2.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA 8002",
    "description": "product-size(500*80),watts(2*1w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA 8002.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA0120A",
    "description": "product-size(95*75*120),watts(5w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA0120A.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA0120C",
    "description": "product-size(95*110*120),watts(5w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA0120C.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA0120D",
    "description": "product-size(95*115*120),watts(10w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA0120D.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA0120E",
    "description": "product-size(95*115*120),watts(10w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA0120E.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA901A",
    "description": "product-size(100*100*100),watts(5w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA901A.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA902A",
    "description": "product-size(100*250*100),watts(2.5w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA902A.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA903",
    "description": "product-size(60*300*35),watts(6w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA903.jpg"
}, {
    "category": "Tires",
    "name": "d'lite LED product",
    "code": "LWA903",
    "description": "product-size(60*300*35),watts(6w), colour(white), colour temperature(3200k)",
    "url": "upload/TiresLWA903.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA903A",
    "description": "product-size(60*295*75),watts(5w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA903A.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA904",
    "description": "product-size(160*70*90),watts(2*3w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA904.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA905",
    "description": "product-size(271*95*100),watts(5w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA905.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA906",
    "description": "product-size(271*95*100),watts(5w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA906.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA908",
    "description": "product-size(160*100*90),watts(5w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA908.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA913",
    "description": "product-size(280*120*105),watts(6w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA913.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA913",
    "description": "product-size(280*120*105),watts(6w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA913.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA921-1",
    "description": "product-size(60*35*100),watts(3w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA921-1.jpg"
}, {
    "category": "LED Products",
    "name": "d'lite LED product",
    "code": "LWA921-2",
    "description": "product-size(60*60*100),watts(6w), colour(white), colour temperature(3200k)",
    "url": "upload/LED ProductsLWA921-2.jpg"
}];

leds.map(function(led){
  $('#leds').append(
  '<div class="col-md-4">' +
  '<div class="card product-info text-center">' +
  '<div class="product-description">' +
  '<img class="img-responsive img-products img-center" src="' + led.url + '" alt="' + led.name + '" />' +
  '<p class="product-description-title">' + led.name + '</p>' +
  '<p class="product-description-code">' + led.code + '</p>' +
  '<p class="product-description-type">' + led.category + '</p>' +
  '<p class="product-description-text">' + led.description + '</p>' +
  '</div>' + '</div>' +
  '</div>');
});
