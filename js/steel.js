var steels = [{
    "category": "Tires",
    "name": "Triangle tire",
    "code": "TR691-JS",
    "description": "Steel Radial Tire",
    "url": "upload/TiresTR691-JS.jpg"
}, {
    "category": "Tires",
    "name": "Triangle tire",
    "code": "TR689",
    "description": "Steel Radial Tire",
    "url": "upload/TiresTR689.jpg"
}, {
    "category": "Tires",
    "name": "Triangle tire",
    "code": "TR 695",
    "description": "Steel Radial Tire",
    "url": "upload/TiresTR 695.jpg"
}, {
    "category": "Tires",
    "name": "Triangle tire",
    "code": "TR 656",
    "description": "Steel Radial Tire",
    "url": "upload/TiresTR 656.jpg"
}, {
    "category": "Tires",
    "name": "Triangle tire",
    "code": "TR 686",
    "description": "Steel Radial Tire",
    "url": "upload/TiresTR 686.jpg"
}, {
    "category": "Tires",
    "name": "Triangle tire",
    "code": "TR 675",
    "description": "Steel Radial Tire",
    "url": "upload/TiresTR 675.jpg"
}, {
    "category": "Tires",
    "name": "Triangle tire",
    "code": "TR 692",
    "description": "Steel Radial Tire",
    "url": "upload/TiresTR 692.jpg"
}, {
    "category": "Tires",
    "name": "Triangle tire",
    "code": "TRD 08",
    "description": "Steel Radial Tire",
    "url": "upload/TiresTRD 08.jpg"
}, {
    "category": "Tires",
    "name": "Triangle tire",
    "code": "TR 668-1",
    "description": "Steel Radial Tire",
    "url": "upload/TiresTR 668-1.jpg"
}, {
    "category": "Tires",
    "name": "Triangle tire",
    "code": "TR 699-JS",
    "description": "Steel Radial Tire",
    "url": "upload/TiresTR 699-JS.jpg"
}, {
    "category": "Tires",
    "name": "Triangle tire",
    "code": "TR 657-JS",
    "description": "Steel Radial Tire",
    "url": "upload/TiresTR 657-JS.jpg"
}, {
    "category": "Tires",
    "name": "Triangle tire",
    "code": "TR 668",
    "description": "Steel Radial Tire",
    "url": "upload/TiresTR 668.jpg"
}, {
    "category": "Tires",
    "name": "Triangle tire",
    "code": "TR-697",
    "description": "Steel Radial Tire",
    "url": "upload/TiresTR-697.jpg"
}, {
    "category": "Tires",
    "name": "Triangle tire",
    "code": "TR-626",
    "description": "Steel Radial Tire",
    "url": "upload/TiresTR-626.jpg"
}, {
    "category": "Tires",
    "name": "Triangle tire",
    "code": "TR 606",
    "description": "Steel Radial Tire",
    "url": "upload/TiresTR 606.jpg"
}, {
    "category": "Tires",
    "name": "Triangle tire",
    "code": "TRY-88",
    "description": "Steel Radial Tire",
    "url": "upload/TiresTRY-88.jpg"
}];

steels.map(function(steel){
  $('#steels').append(
  '<div class="col-md-4">' +
  '<div class="card product-info text-center">' +
  '<div class="product-description">' +
  '<img src="' + steel.url + '" alt="' + steel.name + '" />' +
  '<p class="product-description-title">' + steel.name + '</p>' +
  '<p class="product-description-code">' + steel.code + '</p>' +
  '<p class="product-description-type">' + steel.category + '</p>' +
  '<p class="product-description-text">' + steel.description + '</p>' +
  '</div>' + '</div>' +
  '</div>');
});
