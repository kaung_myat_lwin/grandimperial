var locks = [{
    "category": "Door Locks",
    "name": "Entry Knob-Classic AB (Antique Brass)",
    "code": "8101101",
    "description": "",
    "url": "upload/"
}, {
    "category": "Door Locks",
    "name": "Entry Knob-Classic SC (Satin Chrome)",
    "code": "8101201",
    "description": "",
    "url": "upload/"
}, {
    "category": "Door Locks",
    "name": "Entry Knob-Egg SC (Satin Chrome)",
    "code": "8103201",
    "description": "",
    "url": "upload/"
}, {
    "category": "Door Locks",
    "name": "Entry Knob-Egg AB (Antique Brass)",
    "code": "8103101",
    "description": "",
    "url": "upload/"
}, {
    "category": "Door Locks",
    "name": "Entry Knob-Egg SN(Satin Nickel)",
    "code": "8103301",
    "description": "",
    "url": "upload/"
}, {
    "category": "Door Locks",
    "name": "Entry Knob-Ball AB (Antique Brass)",
    "code": "8102101",
    "description": "",
    "url": "upload/"
}, {
    "category": "Door Locks",
    "name": "Entry Knob-Ball PB (Polished Brass)",
    "code": "8102001",
    "description": "",
    "url": "upload/"
}, {
    "category": "Door Locks",
    "name": "Entry Knob Ball SN (Satin Nickel)",
    "code": "8102301",
    "description": "",
    "url": "upload/"
}, {
    "category": "Door Locks",
    "name": "Lever Entry Curve SN (Satin Nickel)",
    "code": "8107301",
    "description": "",
    "url": "upload/"
}, {
    "category": "Door Locks",
    "name": "Lever Entry Curve PB (Polished Brass)",
    "code": "8107001",
    "description": "",
    "url": "upload/"
}, {
    "category": "Door Locks",
    "name": "Lever Entry-Angle AB (Antique Brassl)",
    "code": "8105101",
    "description": "",
    "url": "upload/"
}, {
    "category": "Door Locks",
    "name": "Lever Entry-Angle SC (Satin Chrome)",
    "code": "8105201",
    "description": "",
    "url": "upload/"
}, {
    "category": "Door Locks",
    "name": "Knob Handleset-Egg SC (Satin Chrome)",
    "code": "8103207",
    "description": "",
    "url": "upload/"
}];

$('#locks').append('lee bal');
