var locks = [{
    "category": "Door Locks",
    "name": "Entry Knob-Classic AB (Antique Brass)",
    "code": "8101101",
    "description": "",
    "url": "upload/honeywell/02.PNG"
}, {
    "category": "Door Locks",
    "name": "Entry Knob-Classic SC (Satin Chrome)",
    "code": "8101201",
    "description": "",
    "url": "upload/honeywell/01.PNG"
}, {
    "category": "Door Locks",
    "name": "Entry Knob-Egg SC (Satin Chrome)",
    "code": "8103201",
    "description": "",
    "url": "upload/honeywell/04.PNG"
}, {
    "category": "Door Locks",
    "name": "Entry Knob-Egg AB (Antique Brass)",
    "code": "8103101",
    "description": "",
    "url": "upload/honeywell/05.PNG"
}, {
    "category": "Door Locks",
    "name": "Entry Knob-Egg SN(Satin Nickel)",
    "code": "8103301",
    "description": "",
    "url": "upload/honeywell/06.PNG"
}, {
    "category": "Door Locks",
    "name": "Entry Knob-Ball AB (Antique Brass)",
    "code": "8102101",
    "description": "",
    "url": "upload/honeywell/07.PNG"
}, {
    "category": "Door Locks",
    "name": "Entry Knob-Ball PB (Polished Brass)",
    "code": "8102001",
    "description": "",
    "url": "upload/honeywell/08.PNG"
}, {
    "category": "Door Locks",
    "name": "Entry Knob Ball SN (Satin Nickel)",
    "code": "8102301",
    "description": "",
    "url": "upload/honeywell/09.PNG"
}, {
    "category": "Door Locks",
    "name": "Lever Entry Curve SN (Satin Nickel)",
    "code": "8107301",
    "description": "",
    "url": "upload/honeywell/010.PNG"
}, {
    "category": "Door Locks",
    "name": "Lever Entry Curve PB (Polished Brass)",
    "code": "8107001",
    "description": "",
    "url": "upload/honeywell/011.PNG"
}, {
    "category": "Door Locks",
    "name": "Lever Entry-Angle AB (Antique Brassl)",
    "code": "8105101",
    "description": "",
    "url": "upload/honeywell/03.PNG"
}, {
    "category": "Door Locks",
    "name": "Lever Entry-Angle SC (Satin Chrome)",
    "code": "8105201",
    "description": "",
    "url": "upload/honeywell/012.PNG"
}, {
    "category": "Door Locks",
    "name": "Knob Handleset-Egg SC (Satin Chrome)",
    "code": "8103207",
    "description": "",
    "url": "upload/honeywell/013.PNG"
}];

locks.map(function(lock){
  $('#locks').append(
  '<div class="col-md-4">' +
  '<div class="card product-info text-center">' +
  '<div class="product-description">' +
  '<img class="img-responsive img-products img-center" src="' + lock.url + '" alt="' + lock.name + '" />' +
  '<p class="product-description-title">' + lock.name + '</p>' +
  '<p class="product-description-code">' + lock.code + '</p>' +
  '<p class="product-description-type">' + lock.category + '</p>' +
  '</div>' + '</div>' +
  '</div>');
});
