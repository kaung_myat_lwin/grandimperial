var exhausts = [{
    "category": "Exhaust Fans & Breakers",
    "name": "Shutter Exhaust Fan",
    "code": "JNB Shutter 1",
    "description": "Shutter Exhaust Fan(6\",8\",10\",12\"),",
    "url": "upload/Exhaust Fans & BreakersJNB Shutter 1.png"
}, {
    "category": "Exhaust Fans & Breakers",
    "name": "Shutter Exhaust Fan",
    "code": "JNB Shutter 2",
    "description": "Shutter Exhaust Fan(8\",10\",12\") Wall Type Net",
    "url": "upload/Exhaust Fans & BreakersJNB Shutter 2.png"
}, {
    "category": "Exhaust Fans & Breakers",
    "name": "Tubular Ventilation Fan",
    "code": "JNB Tubular ",
    "description": "Tubular Ventilation fan(8\",10\",12\")",
    "url": "upload/Exhaust Fans & BreakersJNB Tubular .png"
}, {
    "category": "Exhaust Fans & Breakers",
    "name": "MCB 1P",
    "code": "MCB 1P ",
    "description": "MCB 1P (6A, 10A, 16A, 20A, 32A, 40A)",
    "url": "upload/Exhaust Fans & BreakersMCB 1P .png"
}, {
    "category": "Exhaust Fans & Breakers",
    "name": "MCB 2P",
    "code": "MCB 2P",
    "description": "MCB 2P(16A, 20A, 32A, 40A, 63A)",
    "url": "upload/Exhaust Fans & BreakersMCB 2P.png"
}, {
    "category": "Exhaust Fans & Breakers",
    "name": "MCB 3P",
    "code": "MCB 3P",
    "description": "MCB 3P(20A, 40A, 63A)",
    "url": "upload/Exhaust Fans & BreakersMCB 3P.png"
}];

exhausts.map(function(exhaust){
  $('#exhausts').append(
  '<div class="col-md-4">' +
  '<div class="card product-info text-center">' +
  '<div class="product-description">' +
  '<img class="img-responsive img-products img-center" src="' + exhaust.url + '" alt="' + exhaust.url + '" />' +
  '<p class="product-description-title">' + exhaust.name + '</p>' +
  '<p class="product-description-code">' + exhaust.code + '</p>' +
  '<p class="product-description-type">' + exhaust.category + '</p>' +
  '<p class="product-description-text">' + exhaust.description + '</p>' +
  '</div>' + '</div>' +
  '</div>');
});
