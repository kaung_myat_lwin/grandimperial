var electronics = [{
    "category": "Electronic Products",
    "name": "Washing Machine",
    "code": "GZ70-H5568",
    "description": "Fully Auto Top Load Washing Machine (7 kg)",
    "url": "upload/Electronic ProductsGZ70-H5568.jpg"
}, {
    "category": "Electronic Products",
    "name": "Washing Machine",
    "code": "GZ1072D1-S",
    "description": "Fully Auto Front Load Washing Machine (7 kg)",
    "url": "upload/Electronic ProductsGZ1072D1-S.jpg"
}, {
    "category": "Electronic Products",
    "name": "Washing Machine",
    "code": "GZ109D/1-30",
    "description": "Fully Auto Front Load Washing Machine (9 kg)",
    "url": "upload/Electronic ProductsGZ109D 1-30.jpg"
}, {
    "category": "Electronic Products",
    "name": "Air-Colarr",
    "code": "GZA801Ac",
    "description": "Air-Colarr",
    "url": "upload/Electronic ProductsGZA801Ac.jpg"
}, {
    "category": "Electronic Products",
    "name": "Water Dispenser",
    "code": "GZW-3B-01 HNC",
    "description": "Water Dispenser(Hot,Cold,Normal)Bottom Type(Hidden Style)",
    "url": "upload/Electronic Productswater dispenser.jpg"
}, {
    "category": "Electronic Products",
    "name": "Washing Machine",
    "code": "GZ952-SA",
    "description": "Semi Auto Washing Machine (9.5kg)",
    "url": "upload/Electronic ProductsGZ952-SA.jpg"
}, {
    "category": "Electronic Products",
    "name": "Stand Fan",
    "code": "GZF01SR",
    "description": "Stand Fan",
    "url": "upload/Electronic ProductsGZF01SR.jpg"
}];

electronics.map(function(electronic){
  $('#electronics').append(
  '<div class="col-md-4">' +
  '<div class="card product-info text-center">' +
  '<div class="product-description">' +
  '<img class="img-responsive img-products img-center" src="' + electronic.url + '" alt="' + electronic.name + '" />' +
  '<p class="product-description-title">' + electronic.name + '</p>' +
  '<p class="product-description-code">' + electronic.code + '</p>' +
  '<p class="product-description-type">' + electronic.category + '</p>' +
  '<p class="product-description-text">' + electronic.description + '</p>' +
  '</div>' + '</div>' +
  '</div>');
});
